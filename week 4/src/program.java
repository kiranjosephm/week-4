
public class program {

	public static void main(String[] args) 
	{
		String name = "kiran joseph";
		System.out.println(name);
		
		// 2. Number of characters in the string
		int numChars = name.length();
		System.out.println("Number of characters: " + numChars);
		
		// 3. Get a specific character
		char charat=name.charAt(4);
		System.out.println("char at pos 4: "+ charat);
		
		// 4. Get a substring
		String subn=name.substring(0,5);
		System.out.println("substring 1 is :" +subn);
		String subn1=name.substring(7);
		System.out.println("substring 2 is :" +subn1);
		
		// 5. Check if one string is equal to another
		String a="albert";
		String b="jose";
		String c="albert";
		//compare albert and jose
		if(a.equals(b))
			{System.out.println("a and b are same");}
		else
			{System.out.println("a and b are not same");}
		//compare albert and albert
		if(a.equals(c))
			{System.out.println("a and b are same");}
		else
			{System.out.println("a and b are not same");}
		
		// 6. Make everything uppercase
		String abc="yelling";
		System.out.println(abc.toUpperCase());
		// 7. Make everything lowercase
		String xyz="TADAAAA";
		System.out.println(xyz.toLowerCase());
	}

}
